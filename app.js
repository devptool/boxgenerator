//Invoke functions only after page has fully loaded
window.onload = init;

//Create an array that will be populated by the user generated boxes
var boxes = [];

//Create a global counter variable that keeps track of the number of
//boxes generated
var counter = 0;

var selectBox = "";
var checked = false;

//Create a Box constructor function with parameters, to create box objects
//for each box that's generated
function Box(id, name, color, x, y) {
    this.id = id;
    this.name = name;
    this.color = color;
    this.x = x;
    this.y = y;
}
function getRandomColor() {
    var letters = "0123456789ABCDEF";
    var color = "#";
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
//Set up the onclick event handler for the generate button input
function init() {
    var generateButton = document.getElementById("generateButton");
    generateButton.onclick = generate;

    var clearButton = document.getElementById("clearButton");
    clearButton.onclick = clear;
}

//Get boxes' name from user
function generate() {
    var data = document.forms.data;
    counter = counter + 1;
    //Get color option from user

    var color = getRandomColor();
    if (!color) {
        alert("Pick a color");
        return;
    }

    //Get number of boxes to be generated from user
    var amountArray = [1];
    for (i = 0; i < amountArray.length; i++) {
        //var amount = amountArray[i].value;
        //}
        var scene = document.getElementById("scene");

        //Create and append the new <div> element
        var div = document.createElement("div");

        //Randomly position each <div> element
        var x = Math.floor(Math.random() * (scene.offsetWidth - 101));
        var y = Math.floor(Math.random() * (scene.offsetHeight - 101));

        //Give each <div> element a unique id
        var newId = div;
        newId = counter;
        id = newId;

        //Add the style, including the background color selected
        //by the user.
        div.style.left = x + "px";
        div.style.top = y + "px";
        div.style.backgroundColor = color;
        div.setAttribute("class", "box");
        div.setAttribute("id", "moveBox" + counter);
        scene.appendChild(div);
        div.innerHTML = "Select Me" + "<br />and Move";

        //Create an onclick event displaying the
        //details of each box generated
        div.onclick = function () {
            alert(
                "You clicked on a box with id " +
                id +
                ", whose color is " +
                color +
                " at position " +
                div.style.top +
                ", " +
                div.style.left
            );
            selectBox = div.id;
            console.log(div.id);
        };
        //Form reset
        data = document.getElementById("data");
        data.reset();
    }
}

//Clear the boxes from scene div
function clear() {
    var sceneDivs = document.querySelectorAll("div#scene div");
    for (var i = 0; i < sceneDivs.length; i++) {
        var scene = document.getElementById("scene");
        var cutList = document.getElementsByTagName("div")[1];
        scene.removeChild(cutList);
    }
}

window.addEventListener("keyup", (e) => {
    if (checked) {
        let cutList = document.getElementById(selectBox);
        if (e.key === "Delete") {
            scene.removeChild(cutList);
        }
    }
});

let moveBy = 10;
window.addEventListener("keyup", (e) => {
    if (checked) {
        let box = document.getElementById(selectBox);
        switch (e.key) {
            case "a":
                box.style.left = parseInt(box.style.left) - moveBy + "px";
                break;
            case "d":
                box.style.left = parseInt(box.style.left) + moveBy + "px";
                break;
            case "w":
                box.style.top = parseInt(box.style.top) - moveBy + "px";
                break;
            case "s":
                box.style.top = parseInt(box.style.top) + moveBy + "px";
                break;
        }
    }
});

function checkbox() {
    if (document.querySelector("#opt1:checked")) {
        checked = true;
    } else {
        checked = false;
    }
    document.getElementById("msg").innerText = checked;
    console.log(document.getElementById("msg"));
}
